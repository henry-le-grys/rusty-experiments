#![feature(str_split_once)]
extern crate glib;
extern crate gstreamer;

use std::thread;
use std::time::Duration;

use glib::GBoxed;
use glib::subclass::prelude::*;
use gstreamer::{Bus, CLOCK_TIME_NONE, Element, ElementFactory, MessageView, Pipeline, State, Structure};
use gstreamer::event::Eos;
use gstreamer::message::Application;
use gstreamer::prelude::*;

mod server;

#[derive(Clone, Debug, Eq, PartialEq)]
enum AppMessage {
    Play,
    Pause,
    SetSrc(String),
}

#[derive(Clone, Debug, Eq, PartialEq, GBoxed)]
#[gboxed(type_name = "BoxedMessage")]
struct BoxedMessage(AppMessage);

fn post_message(bus: &Bus, message: AppMessage) {
    let app_msg = BoxedMessage(message);
    let st = Structure::builder("AppMessage")
        .field("message", &app_msg.to_send_value())
        .build();
    let msg = Application::builder(st)
        .build();

    if let Err(e) = bus.post(&msg) {
        eprintln!("Bus error: {}", e.message);
    };
}

fn main() {
    gstreamer::init().unwrap();

    let src = ElementFactory::make("filesrc", "source".into()).unwrap();
    let decoder = ElementFactory::make("decodebin", "decoder".into()).unwrap();

    let audio_convert = ElementFactory::make("audioconvert", "audio-conv".into()).unwrap();
    let audio_resample = ElementFactory::make("audioresample", "audio-resample".into()).unwrap();
    let audio_sink = ElementFactory::make("autoaudiosink", "audio-sink".into()).unwrap();

    let video_convert = ElementFactory::make("videoconvert", "video-conv".into()).unwrap();
    let video_sink = ElementFactory::make("ximagesink", "video-sink".into()).unwrap();

    let pipeline = Pipeline::new("test-pipeline".into());
    pipeline.add_many(&[&src, &decoder, &audio_convert, &audio_resample, &audio_sink, &video_convert, &video_sink])
        .expect("Could not create pipeline.");

    src.link(&decoder).expect("Could not link src->decoder");
    video_convert.link(&video_sink).expect("Could not link video_conv->sink");
    Element::link_many(&[&audio_convert, &audio_resample, &audio_sink])
        .expect("Could not link audio pipeline");

    decoder.connect_pad_added(move |src, pad| {
        println!("Received new pad '{}' from '{}'", pad.get_name(), src.get_name());

        let caps = pad.get_current_caps().expect("Cannot get caps of pad.");
        let cap_structure = caps.get_structure(0).expect("Cannot get structure of caps.");
        let pad_type = cap_structure.get_name();

        println!("Pad type: {}", pad_type);

        if pad_type == "video/x-raw" {
            src.link(&video_convert).expect("Could not link video sink.");
        } else {
            src.link(&audio_convert).expect("Could not link audio sink.");
        }
    });

    pipeline.set_state(State::Ready).expect("Could not ready pipeline");

    let bus = pipeline.get_bus().expect("Could not get bus");
    let bus2 = bus.clone();
    thread::spawn(move || {
        server::start_server(move |command| {
            if command == "play" {
                post_message(&bus2, AppMessage::Play);
            } else if command == "pause" {
                post_message(&bus2, AppMessage::Pause);
            } else if command.starts_with("setsrc:") {
                let (_, location) = command.split_once(":").unwrap();

                if !location.is_empty() {
                    post_message(&bus2, AppMessage::SetSrc(location.into()));
                }
            }
        });
    });

    for msg in bus.iter_timed(CLOCK_TIME_NONE) {
        match msg.view() {
            MessageView::Error(err) => {
                eprintln!("Error received from element {}: {}",
                          err.get_src().map(|x| x.get_path_string()).unwrap(),
                          err.get_error());
                break;
            }

            MessageView::StateChanged(state) => {
                if state.get_src().map(|src| src == pipeline).unwrap_or(false) {
                    println!("Pipeline state changed, {:?} => {:?}", state.get_old(), state.get_current());
                }
            }

            MessageView::Eos(_) => break,
            MessageView::Application(app_msg) => {
                let st = app_msg.get_structure().unwrap();
                let msg: &BoxedMessage = st.get("message").unwrap().unwrap();

                match &msg.0 {
                    AppMessage::Play => {
                        if let Err(e) = pipeline.set_state(State::Playing) {
                            eprintln!("{}", e);
                        }
                    },
                    AppMessage::Pause => {
                        if let Err(e) = pipeline.set_state(State::Paused) {
                            eprintln!("{}", e);
                        }
                    },
                    AppMessage::SetSrc(location) => {
                        if let Err(e) = pipeline.set_state(State::Ready) {
                            eprintln!("{}", e);
                        };
                        if let Err(e) = src.set_property("location", location) {
                            eprintln!("{}", e.message);
                        }
                    }
                };

                ()
            }
            _ => (),
        }
    }

    pipeline.set_state(State::Null).unwrap();
}
