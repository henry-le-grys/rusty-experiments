use std::io::{BufRead, BufReader, ErrorKind, Read};
use std::net::{TcpListener, TcpStream};

fn handle_client<F: Fn(String) -> ()>(mut stream: TcpStream, cb: F) {
    let mut reader = BufReader::new(stream);

    loop {
        let mut body = String::new();
        if let Err(e) = reader.read_line(&mut body) {
            eprintln!("{}", e);
            break
        }

        body = String::from(body.trim());

        println!("{}", body);
        cb(body);
    }

    // let mut len_buf = [0; 4];
    // reader.read_exact(&mut len_buf);
    //
    // let len = u32::from_le_bytes(len_buf);
    // let mut buf = Vec::<u8>::with_capacity(len as usize);
    // buf.resize(len as usize, 0);
    //
    // reader.read(&mut buf);
}

pub fn start_server<F: Fn(String) -> ()>(cb: F) {
    let listener = TcpListener::bind("127.0.0.1:7000").expect("Failed to bind to socket!");

    for stream in listener.incoming() {
        if let Ok(client) = stream {
            handle_client(client, &cb);
        }
    }

    ()
}
